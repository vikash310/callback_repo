
import json
import os
from datetime import datetime
from flask import Flask, request
from requests import post
from xpms_storage.db_handler import DBProvider

app = Flask(__name__)
API_GATEWAY_URL = os.environ["API_GATEWAY_URL"]


@app.route("/external_api", methods=["POST"])
def external_api_call():
    data = request.get_json()
    solution_id = request.headers["solution_id"]
    user_id = request.headers["user_id"]
    service_url = API_GATEWAY_URL + "/entity/config/save"
    payload = {"solution_id": solution_id, "user_id": user_id,
               "callback_url": "http://0.0.0.0:3000/notification_response",
               "data": {"entity_cfg": data["entity_cfg"]}}
    response_data = post(service_url, json.dumps(payload), headers={'Content-Type': 'application/json'})
    res_data = response_data.json()
    payload.update({"job_id": res_data["job_id"]})
    db = DBProvider.get_instance("system")
    db.insert(table="callback_coll", rows=payload, dup_update=True)
    return True


@app.route("/notification_response", methods=["POST"])
def response_from_notification():
    response_data = request.get_json()
    db = DBProvider.get_instance("system")
    db.insert(table="callback_coll", rows=response_data, dup_update=True)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000, debug=True)
